/*
 * Copyright © 2020 by Fahd Tauil
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

/**
 * \file queue.c
 *
 * \date Mar 1, 2020
 *
 * \author Fahd Tauil (tauil.fahd@gmail.com)
 *
 * \brief this file contains the queue module implementation as well as the internal type definitions
 *
 */

#include <string.h>
#include "queue_cfg.h"
#include "queue.h"

#ifndef MAX_QUEUE_NUMBER
#error "Compiler ERROR: please define the maximum number of queues !!!"
#endif

/*
 *************************************************************************************************************
 *                                        M A C R O S   D E F I N I T I O N                                  *
 *************************************************************************************************************
 */
#define GetQueuesCount_mac()     (queue_main_config_mst.queues_count)
#define IncQueuesCount_mac()     (queue_main_config_mst.queues_count++)
#define DecQueuesCount_mac()     (queue_main_config_mst.queues_count--)

#define GetQueueByNumber_mac(n)    (queue_main_config_mst.queues_mpst[(n)])

#define GetQueueDataSize_mac(n)    ( queue_main_config_mst.queue_internal[(n)].data_size )

#define GetDataOffsetPtr_mac(n, o) \
  ( (uint8_t*)queue_main_config_mst.queues_mpst[(n)].queue_table + (GetQueueDataSize_mac(n) * (o)) )

#define GetQueueNextPush_mac(n)      ( queue_main_config_mst.queues_mpst[(n)].next_push )

#define GetQueueNextPop_mac(n)       ( queue_main_config_mst.queues_mpst[(n)].next_pop )

#define GetQueueState_mac(n)         ( queue_main_config_mst.queues_mpst[(n)].queue_state)

#define GetQueueSize_mac(n)          ( queue_main_config_mst.queue_internal[(n)].queue_size )

#define GetQueueAvailability_mac(n)  ( queue_main_config_mst.queue_internal[(n)].is_active )

#define IncQueueNextPush_mac(n) ( GetQueueNextPush_mac(n) = (GetQueueNextPush_mac(n) + 1) %  GetQueueSize_mac(n) )

#define IncQueueNextPop_mac(n)  ( GetQueueNextPop_mac(n) = (GetQueueNextPop_mac(n) + 1) %  GetQueueSize_mac(n) )

#define SetQueueState_mac(n, s)        ( queue_main_config_mst.queues_mpst[(n)].queue_state = (s) )

#define SetQueueDataSize_mac(n, s)     ( queue_main_config_mst.queue_internal[(n)].data_size = (s) )

#define SetQueueAvailability_mac(n, a) ( queue_main_config_mst.queue_internal[(n)].is_active = (a) )

#define SetQueueSize_mac(n, s)      ( queue_main_config_mst.queue_internal[(n)].queue_size = (s) )

#define SetQueueTablePtr_mac(n, p)  ( queue_main_config_mst.queues_mpst[(n)].queue_table = (p) )

#define SetQueueNextPop_mac(n, v)   ( queue_main_config_mst.queues_mpst[(n)].next_pop = (v) )

#define SetQueueNextPush_mac(n, v)  ( queue_main_config_mst.queues_mpst[(n)].next_push = (v) )


/*
 *************************************************************************************************************
 *                              F U N C T I O N S   P R O T O T Y P E S                                      *
 *************************************************************************************************************
 */

VISIBILITY StdReturn_gui8_t search_free_queue_mui8(uint32_t * available_queue_fui8);

StdReturn_gui8_t create_queue_gui8 ( uint32_t queue_size_fu32, uint32_t data_size_fu32, void * queue_table_fpvd,
    QueueToken_gst_t * queue_token_fpst );

StdReturn_gui8_t delete_queue_gui8 ( const QueueToken_gst_t * queue_token_fst );

StdReturn_gui8_t queue_push_gui8 ( const QueueToken_gst_t * queue_token_fpst, void * const data_fpvd );

StdReturn_gui8_t queue_pop_gui8 ( const QueueToken_gst_t * queue_token_fpst, void *data_fpvd );

StdReturn_gui8_t get_queue_state_gui8 ( const QueueToken_gst_t * queue_token_fpst, QueueState_genm_t * queue_state_fpenm );

StdReturn_gui8_t get_queue_usage_gui8 ( const QueueToken_gst_t * queue_token_fpst, uint32_t * entries_count_fpu32 );

StdReturn_gui8_t get_queue_free_entries_gui8 ( const QueueToken_gst_t *  queue_token_fpst, uint32_t * rem_entries_fpu32 );

/*
 *************************************************************************************************************
 *                             I N T E R N A L   T Y P E S   D E F I N I T I O N                             *
 *************************************************************************************************************
 */

typedef struct _QueueData_mst_t {
  uint8_t * queue_table;
  uint32_t next_push;
  uint32_t next_pop;
  QueueState_genm_t queue_state;
} QueueData_mst_t;

typedef struct _QueueInternalData_mst_t {
  uint32_t data_size;
  uint32_t queue_size;
  Boolean is_active;
} QueueInternalData_mst_t;

typedef struct _QueueMainConfig_mst_t {
  QueueData_mst_t queues_mpst[ MAX_QUEUE_NUMBER ];
  QueueInternalData_mst_t queue_internal[ MAX_QUEUE_NUMBER ];
  uint32_t queues_count;
  Boolean queues_initialized;
} QueueMainConfig_mst_t;


/*
 *************************************************************************************************************
 *                             I N T E R N A L   V A R I A B L E S   D E F I N I T I O N S                   *
 *************************************************************************************************************
 */
QueueMainConfig_mst_t queue_main_config_mst;

/*
 *************************************************************************************************************
 *                             P U B L I C   F U N C T I O N S   D E F I N I T I O N S                       *
 *************************************************************************************************************
 */

/*
 * create_queue_gui8 function: full description can be found in the header file.
 */
StdReturn_gui8_t create_queue_gui8 ( uint32_t queue_size_fu32, uint32_t data_size_fu32, void * queue_table_fpvd,
    QueueToken_gst_t * queue_token_fpst )
{
  StdReturn_gui8_t result_lui8 = OP_NOK;
  uint32_t available_queue_number_lu32 = 0;
  if ( ( queue_size_fu32 != 0 ) && ( data_size_fu32 != 0 ) ) {
    if ( queue_table_fpvd != NULL_PTR ) {
      if ( GetQueuesCount_mac() < MAX_QUEUE_NUMBER ) {
        result_lui8 = search_free_queue_mui8 ( &available_queue_number_lu32 );
        if ( result_lui8 == OP_OK ) {
          SetQueueTablePtr_mac( available_queue_number_lu32 , (uint8_t* )queue_table_fpvd );
          SetQueueState_mac( available_queue_number_lu32 , QUEUE_EMPTY_enm );
          SetQueueDataSize_mac( available_queue_number_lu32 , data_size_fu32 );
          SetQueueSize_mac( available_queue_number_lu32 , queue_size_fu32 );
          SetQueueAvailability_mac( available_queue_number_lu32 , TRUE );
          queue_token_fpst->queue_number = available_queue_number_lu32;
          IncQueuesCount_mac();
          result_lui8 = OP_OK;
        } else {
          // no free queue available, this part should be never reach
          result_lui8 = OP_NOK;
        }
      } else {
        // no more queues are possible
        result_lui8 = OP_NOK;
      }
    } else {
      // table pointer is NULL
      result_lui8 = OP_NOK;
    }
  } else {
    //data or queue sizes are equal to zero
    result_lui8 = OP_NOK;
  }
  return result_lui8;
}

/*
 * delete_queue_gui8 function: full description can be found in the header file.
 */
StdReturn_gui8_t delete_queue_gui8 ( const QueueToken_gst_t * queue_token_fst )
{
  StdReturn_gui8_t ret_lui8 = OP_NOK;
  uint32_t queue_number_lu32 = queue_token_fst->queue_number;

  if ( queue_number_lu32 < MAX_QUEUE_NUMBER ) {
    if ( GetQueueAvailability_mac(queue_number_lu32) == TRUE ) {
      SetQueueAvailability_mac( queue_number_lu32 , FALSE );
      SetQueueDataSize_mac( queue_number_lu32 , 0 );
      SetQueueSize_mac( queue_number_lu32 , 0 );
      SetQueueTablePtr_mac( queue_number_lu32 , NULL_PTR );
      SetQueueNextPush_mac( queue_number_lu32 , 0 );
      SetQueueNextPop_mac( queue_number_lu32 , 0 );
      SetQueueState_mac( queue_number_lu32 , QUEUE_INIT_enm );
      DecQueuesCount_mac();
      ret_lui8 = OP_OK;
    } else {
      // queue already deleted or not created yet
      ret_lui8 = OP_NOK;
    }
  } else {
    // queue number more that MAX_QUEUE_NUMNER
    ret_lui8 = OP_NOK;
  }
  return ret_lui8;
}

/*
 *  queue_push_gui8 function: full description can be found in the header file.
 */
StdReturn_gui8_t queue_push_gui8 ( const QueueToken_gst_t * queue_token_fpst, void * const data_fpvd )
{
  StdReturn_gui8_t ret_lui8 = OP_NOK;
  uint8_t * aux_ptr_lpui8 = NULL_PTR;
  uint32_t queue_number_lu32;

  if ( ( queue_token_fpst != NULL_PTR ) && ( data_fpvd != NULL_PTR ) ) {
    queue_number_lu32 = queue_token_fpst->queue_number;
    if ( GetQueueAvailability_mac(queue_number_lu32) == TRUE ) {
      if ( ( GetQueueState_mac(queue_number_lu32) == QUEUE_EMPTY_enm )
          || ( GetQueueState_mac(queue_number_lu32) == QUEUE_RUNING_enm ) ) {
        //copy the data to queue
        aux_ptr_lpui8 = GetDataOffsetPtr_mac( queue_number_lu32 , GetQueueNextPush_mac(queue_number_lu32) );
        memcpy ( (uint8_t*) aux_ptr_lpui8 , (uint8_t*) data_fpvd , GetQueueDataSize_mac( queue_number_lu32 ) );
        IncQueueNextPush_mac( queue_number_lu32 );
        if ( GetQueueNextPop_mac(queue_number_lu32) == GetQueueNextPush_mac( queue_number_lu32 ) ) {
          SetQueueState_mac( queue_number_lu32 , QUEUE_FULL_enm );
        } else {
          SetQueueState_mac( queue_number_lu32 , QUEUE_RUNING_enm );
        }
        ret_lui8 = OP_OK;
      } else {
        // queue is full
        ret_lui8 = OP_NOK;
      }
    } else {
      // Queue is not available or deleted
      ret_lui8 = OP_NOK;
    }
  } else {
    // null pointer
    ret_lui8 = OP_NOK;
  }
  return ret_lui8;
}

/*
 * queue_pop_gui8 function: full description can be found in the header file.
 */
StdReturn_gui8_t queue_pop_gui8 ( const QueueToken_gst_t * queue_token_fpst, void *data_fpvd )
{
  StdReturn_gui8_t ret_lui8 = OP_NOK;
  uint8_t *aux_ptr_lpui8 = NULL_PTR;
  uint32_t queue_number_lu32;

  if ( ( queue_token_fpst != NULL_PTR ) && ( data_fpvd != NULL_PTR ) ) {
    queue_number_lu32 = queue_token_fpst->queue_number;
    if ( GetQueueAvailability_mac(queue_number_lu32) == TRUE ) {
      if ( ( GetQueueState_mac(queue_number_lu32) == QUEUE_FULL_enm ) || GetQueueState_mac(queue_number_lu32) == QUEUE_RUNING_enm ) {
        // copy data from the queue to the pointer
        aux_ptr_lpui8 = GetDataOffsetPtr_mac( queue_number_lu32 , GetQueueNextPop_mac(queue_number_lu32) );
        memcpy ( (uint8_t*) data_fpvd , (uint8_t*) aux_ptr_lpui8 , GetQueueDataSize_mac( queue_number_lu32 ) );
        IncQueueNextPop_mac( queue_number_lu32 );
        if ( GetQueueNextPop_mac(queue_number_lu32) == GetQueueNextPush_mac( queue_number_lu32 ) ) {
          SetQueueState_mac( queue_number_lu32 , QUEUE_EMPTY_enm );
        } else {
          SetQueueState_mac( queue_number_lu32 , QUEUE_RUNING_enm );
        }
        ret_lui8 = OP_OK;
      } else {
        // queue empty
        ret_lui8 = OP_NOK;
      }
    }

  } else {
    // null pointers
    ret_lui8 = OP_NOK;
  }
  return ret_lui8;
}

StdReturn_gui8_t get_queue_state_gui8 ( const QueueToken_gst_t * queue_token_fpst, QueueState_genm_t *queue_state_fpenm )
{
  StdReturn_gui8_t result_lui8 = OP_NOK;

  if ( ( queue_token_fpst != NULL_PTR ) && ( queue_state_fpenm != NULL_PTR ) ) {
    if ( GetQueueAvailability_mac( queue_token_fpst->queue_number ) ) {
      *queue_state_fpenm = GetQueueState_mac( queue_token_fpst->queue_number );
      result_lui8 = OP_OK;
    } else {
      // queue is no available (not created yet)
      result_lui8 = OP_NOK;
    }
  } else {
    // null pointers
    result_lui8 = OP_NOK;
  }
  return result_lui8;
}

StdReturn_gui8_t get_queue_usage_gui8 ( const QueueToken_gst_t * queue_token_fpst, uint32_t *entries_count_fpu32 )
{
  StdReturn_gui8_t result_lui8 = OP_NOK;
  uint32_t queue_nbr_lu32 = 0;
  uint32_t next_push_u32 = 0;
  uint32_t next_pop_u32 = 0;
  QueueState_genm_t queue_state_lenm;

  if ( ( queue_token_fpst != NULL_PTR ) && ( entries_count_fpu32 != NULL_PTR ) ) {
    queue_nbr_lu32 = queue_token_fpst->queue_number;
    if ( GetQueueAvailability_mac(queue_nbr_lu32) == TRUE ) {
      next_push_u32 = GetQueueNextPush_mac( queue_nbr_lu32 );
      next_pop_u32 = GetQueueNextPop_mac( queue_nbr_lu32 );
      if ( next_push_u32 > next_pop_u32 ) {
        *entries_count_fpu32 = next_push_u32 - next_pop_u32;
      } else if ( next_push_u32 < next_pop_u32 ) {
        *entries_count_fpu32 = GetQueueSize_mac(queue_nbr_lu32) - next_pop_u32 + next_push_u32;
      } else {
        // queue is empty or full
        (void) get_queue_state_gui8 ( queue_token_fpst , &queue_state_lenm );
        if ( queue_state_lenm == QUEUE_EMPTY_enm ) {
          *entries_count_fpu32 = 0;
        } else if ( queue_state_lenm == QUEUE_FULL_enm ) {
          *entries_count_fpu32 = GetQueueSize_mac( queue_nbr_lu32 );
        } else {
          *entries_count_fpu32 = 0;
        }
      }
      result_lui8 = OP_OK;
    } else {
      // queue not yet created
      result_lui8 = OP_NOK;
    }
  } else {
    // null pointer
    *entries_count_fpu32 = 0;
    result_lui8 = OP_NOK;
  }
  return result_lui8;
}

StdReturn_gui8_t get_queue_free_entries_gui8 ( const QueueToken_gst_t * queue_token_fpst, uint32_t * rem_entries_fpu32 )
{
  StdReturn_gui8_t result_lui8 = OP_NOK;
  uint32_t queue_nbr_u32 = 0;
  uint32_t used_entries_u32 = 0;

  if ( ( queue_token_fpst != NULL_PTR ) && ( rem_entries_fpu32 != NULL_PTR ) ) {
    queue_nbr_u32 = queue_token_fpst->queue_number;
    result_lui8 = get_queue_usage_gui8 ( queue_token_fpst , &used_entries_u32 );
    if ( result_lui8 == OP_OK ) {
      *rem_entries_fpu32 = GetQueueSize_mac(queue_nbr_u32) - used_entries_u32;
      result_lui8 = OP_OK;
    } else {
      // queue not available
      result_lui8 = OP_NOK;
    }
  } else {
    // pointers are null
    result_lui8 = OP_NOK;
  }
  return result_lui8;
}

/*
 *************************************************************************************************************
 *                        S T A T I C   F U N C T I O N S   D E F I N I T I O N S                            *
 *************************************************************************************************************
 */

/**
 *
 * \brief search_free_queue_mui8 is used to search the first available free queue.
 *
 * \return StdReturn_gui8_t OP_OK if a free queue has been found, OP_NOK instead.
 *
 * \param OUTPUT available_queue_fui8 will contain the queue index
 *
 */
VISIBILITY StdReturn_gui8_t search_free_queue_mui8 ( uint32_t * available_queue_fui8 )
{
  StdReturn_gui8_t ret_lui8 = OP_NOK;
  uint8_t loop_lui8;
  for ( loop_lui8 = 0; loop_lui8 < MAX_QUEUE_NUMBER; loop_lui8++ ) {
    if ( queue_main_config_mst.queue_internal[ loop_lui8 ].is_active == FALSE ) {
      *available_queue_fui8 = (uint32_t) loop_lui8;
      ret_lui8 = OP_OK;
      break;
    }
  }
  return ret_lui8;
}
// end of queue.c
