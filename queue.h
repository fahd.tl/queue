/*
 * Copyright © 2020 by Fahd Tauil
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

/**
 * \file queue.h
 *
 * \date Mar 1, 2020
 *
 * \author Fahd Tauil (tauil.fahd@gmail.com)
 *
 * \brief this file contain all the public types and function that can be used by other modules for the
 *        queue implementation.
 */

#ifndef QUEUE_H_
#define QUEUE_H_

#include "std_types.h"

#ifndef VISIBILITY
#define VISIBILITY static
#endif

/*
 *************************************************************************************************************
 *                            E X T E R N A L   T Y P E   D E F I N T I O N S                                *
 *************************************************************************************************************
 */
typedef struct _QueueToken_gst_t
{
  uint32_t queue_number;
} QueueToken_gst_t;

typedef enum _QueueState_genm_t
{
  QUEUE_INIT_enm = 0, /**< Queue still not created */
  QUEUE_EMPTY_enm,    /**< Queue is created, with a token */
  QUEUE_RUNING_enm,
  QUEUE_FULL_enm
} QueueState_genm_t;

/*
 *************************************************************************************************************
 *                              F U N C T I O N S   P R O T O S T Y P E S                                    *
 *************************************************************************************************************
 */
/**
 * \brief Function that create a new queue.
 *
 * \return OP_OK if queue created successfully; OP_NOK if not.
 *
 * \param [in] queue_size_fu32  queue number of elements.
 *
 * \param [in] data_size_fu32 data size.
 *
 * \param [in] queue_table_fpvd pointer to the actual queue table.
 *
 * \param [out] queue_token_fpst token given to the created queue.
 */
extern StdReturn_gui8_t create_queue_gui8 ( uint32_t queue_size_fu32, uint32_t data_size_fu32, void * queue_table_fpvd,
    QueueToken_gst_t * queue_token_fpst );

/**
 * \brief queue_push_gui8 used to push new data to the queue.
 *
 * \return OP_OK if the data has been pushed correctly to the queue, OP_NOK instead.
 *
 * \param [in] queue_token_fpst token represent the wanted queue.
 *
 * \param [in] data_fpvd data to be pushed.
 */
extern StdReturn_gui8_t queue_push_gui8 ( const QueueToken_gst_t * queue_token_fpst, void * const data_fpvd );

/**
 * \brief queue_pop_gui8 used to pop data from the queue.
 *
 * \return OP_OK if the data has been popped correctly from the queue, OP_NOK instead.
 *
 * \param [in] queue_token_fpst token represent the wanted queue.
 *
 * \param [out] data_fpvd pointer to the array that will contain the data.
 */
extern StdReturn_gui8_t queue_pop_gui8 ( const QueueToken_gst_t * queue_token_fpst, void *data_fpvd );

/**
 * \brief delete_queue_gui8 used to delete the queue.
 *
 * \return OP_OK if the queue has been successfully deleted, OP_NOK instead.
 *
 * \param [in] queue_token_fpst token represent the wanted queue.
 */
extern StdReturn_gui8_t delete_queue_gui8 ( const QueueToken_gst_t * queue_token_fst );

/**
 * \brief get_queue_state_gui8 used to return the queue state.
 *
 * \return OP_OK if the the input pointers are not null and queue is available, OP_NOK instead.
 *
 * \param [in] queue_token_fpst token represent the wanted queue.
 *
 * \param [out] queue_state_fpenm pointer that will hold the state of the queue
 */
extern StdReturn_gui8_t get_queue_state_gui8 ( const QueueToken_gst_t * queue_token_fpst, QueueState_genm_t * queue_state_fpenm );

/**
 * \brief get_used_entries_gui8 used to get number of elements in the queue.
 *
 * \return OP_OK if the the input pointers are not null and queue is available, OP_NOK instead.
 *
 * \param [in] queue_token_fpst token represent the wanted queue.
 *
 * \param [out] queue_token_fpst pointer that will hold the entries count.
 */
extern StdReturn_gui8_t get_queue_usage_gui8 ( const QueueToken_gst_t * queue_token_fpst, uint32_t * entries_count_fpu32 );

/**
 * \brief get_free_entries_gui8 used to get the remaining entries still to be filled.
 *
 * \return OP_OK if the the input pointers are not null and queue is available, OP_NOK instead.
 *
 * \param [in] queue_token_fpst token represent the wanted queue.
 *
 * \param [out] queue_token_fpst pointer that will hold the remaining entries count.
 */
extern StdReturn_gui8_t get_queue_free_entries_gui8 ( const QueueToken_gst_t * queue_token_fpst, uint32_t * rem_entries_fpu32 );



#endif /* QUEUE_H_ */
